import { push } from 'react-router-redux'
import { setActiveLanguage } from 'react-localize-redux'

const syncRoutesToLocale = store => next => action => {
    // todo filter for location changes that change to locale path
    if(action.type === "router/LOCATION_CHANGE") {
        store.dispatch(setActiveLanguage(action.payload.languageCode))
    }

    return next(action)
}

export default syncRoutesToLocale
