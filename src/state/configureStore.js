import { createStore, combineReducers, applyMiddleware } from "redux"
import { localeReducer as locale } from 'react-localize-redux';
import { routerReducer, routerMiddleware } from 'react-router-redux'
import { composeWithDevTools } from 'redux-devtools-extension';

// todo put this back in
// import syncRoutesToLocale from './reducers/syncRoutesToLocale'

const configureStore = (history) => {
    return createStore(
        combineReducers({
            locale,
            router: routerReducer,
            // syncRoutesToLocale
        }),
        // TODO Add redux devtool middleware
        composeWithDevTools(
            applyMiddleware(
                routerMiddleware(history),
            )
        )
    )
}

export default configureStore
