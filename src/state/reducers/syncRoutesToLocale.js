import { push } from 'react-router-redux'
import { getActiveLanguage } from 'react-localize-redux'

const TOGGLE_LANGUAGE = 'language/TOGGLE_LANGUAGE'

const toggleLanguage = (actionType) => {
    switch (actionType) {
        case TOGGLE_LANGUAGE: {
            return {
                type: TOGGLE_LANGUAGE,
                payload: {

                }
            }
        }
        default:
    }
}

const syncRoutesToLocale = store => next => action => {
    if(action.type === "@@localize/SET_ACTIVE_LANGUAGE") {
        store.dispatch(push(action.payload.languageCode))
    }

    return next(action)
}

export default syncRoutesToLocale
