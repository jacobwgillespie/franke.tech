import React from 'react';
import { connect } from 'react-redux'
import { getTranslate, getActiveLanguage } from 'react-localize-redux'
import {
  Avatar,
  Text,
  Heading,
  Container,
  Flex,
  Box,
  Link
} from 'rebass';

import {
  FaStackOverflow,
  FaXing,
  FaLinkedin,
  FaGithub,
  FaFilePdfO
} from 'react-icons/lib/fa';

import portrait from '../portrait.jpg'

const IconLink = Link.extend`
  width: 50px;
  height: 50px;
  padding: 14px;
  margin: 10px 5px;
  border-radius: 4px;
  background-color: lightgrey;
  
  :hover {
    background-color: gray;
  }
`

const IndexPage = ({ translate, activeLanguage }) => (
  <Flex justifyContent='center'>
    <Container m="auto" maxWidth={600}>
      <Flex my={[1, 2, 3, 4]} justifyContent='center'>
        <Box width={[4 / 5, 1 / 2, 400]}>
          <Avatar
            src={portrait}
            alt="Benedikt Franke"
            size="100%"
          />
        </Box>
      </Flex>
      <Heading.h1 textAlign="center" fontSize={6}>Benedikt Franke</Heading.h1>
      <Text is="h2" fontWeight="normal" textAlign="center" fontSize={4}>{translate('jobtitle')}</Text>
      <Flex justifyContent='center'>
        {
          activeLanguage === 'de' &&
          <IconLink target="_blank" href="https://www.xing.com/profile/Benedikt_Franke8" title="Xing">
            <FaXing size="100%" color="#026466" />
          </IconLink>
        }
        <IconLink target="_blank" href={translate('links.linkedIn')} title="LinkedIn">
          <FaLinkedin size="100%" color="#0077b5" />
        </IconLink>
        <IconLink target="_blank" href="https://stackoverflow.com/story/spawnia" title="Stack Overflow">
          <FaStackOverflow size="100%" color="#f48024" />
        </IconLink>
        <IconLink target="_blank" href="https://github.com/spawnia" title="GitHub">
          <FaGithub size="100%" color="#333" />
        </IconLink>
        <IconLink target="_blank" href={translate('resume.link')} title={translate('resume.title')}>
          <FaFilePdfO size="100%" color="#f22" />
        </IconLink>
      </Flex>
      <Text textAlign="center" fontSize={2}>{translate('motto')}</Text>
    </Container>
  </Flex>
)

const mapStateToProps = state => ({
  translate: getTranslate(state.locale),
  activeLanguage: getActiveLanguage(state.locale).code
});

export default connect(mapStateToProps)(IndexPage);
