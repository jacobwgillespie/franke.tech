import React, {Fragment} from 'react'
import Helmet from 'react-helmet'
import { getTranslate } from 'react-localize-redux';
import { connect } from 'react-redux'

import IndexPage from './pages'
import LanguageToggle from './components/LanguageToggle';

const App = props => {
  const { translate } = props
  return (
    <Fragment>
      <Helmet
        title={"Benedikt Franke - " + translate('jobtitle')}
        meta={[
          { name: 'description', content: 'Benedikt Franke is a software engineer who specialises in DevOps and web development.' },
          { name: 'keywords', content: 'web developer, software engineer, devops, web, software' },
        ]}
      />
      <LanguageToggle />
      <IndexPage />
    </Fragment>
  )
}

export default connect(
  state => ({
    translate: getTranslate(state.locale),
  }))(App)
