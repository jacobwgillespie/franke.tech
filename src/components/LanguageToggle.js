import React from 'react'
import { connect } from 'react-redux'
import { getActiveLanguage, setActiveLanguage } from 'react-localize-redux'
import {
    Switch,
    Text,
    Flex,
} from 'rebass';

const LanguageToggle = ({ setActiveLanguage, activeLanguage }) => {

    const currentlyEnglish = activeLanguage === 'en'

    const toggleLanguage = () => {
        currentlyEnglish ?
            setActiveLanguage('de')
            : setActiveLanguage('en')
    }

    const LanguageSwitch = Switch.extend`
        cursor: pointer;
    `

    const LanguageLabel = Text.extend`
        color: ${props => props.active ? 'black' : 'gray'};
        text-shadow: ${props => props.active ? '0 0 .65px #333' : 'normal'};
        cursor: pointer;
        border-radius: 4px;
        padding: 8px 16px;
        :hover {
            box-shadow: inset 0 0 0 999px rgba(0,0,0,0.125);
        }
    `

    return (
        <Flex alignItems='baseline' justifyContent='center'>
            <LanguageLabel active={currentlyEnglish} onClick={() => setActiveLanguage('en')}>English</LanguageLabel>
            <LanguageSwitch m={2} onClick={toggleLanguage} checked={!currentlyEnglish} />
            <LanguageLabel active={!currentlyEnglish} onClick={() => setActiveLanguage('de')}>Deutsch</LanguageLabel>
        </Flex>
    )
}

const mapStateToProps = state => ({ activeLanguage: getActiveLanguage(state.locale).code });
export default connect(mapStateToProps, { setActiveLanguage })(LanguageToggle);
