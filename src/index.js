import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Provider as ThemeProvider } from 'rebass'

import createHistory from 'history/createBrowserHistory'
import { ConnectedRouter } from 'react-router-redux';


import { initialize, addTranslation } from 'react-localize-redux';

import App from './App';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import configureStore from './state/configureStore';

const history = createHistory()

const store = configureStore(history)


store.dispatch(initialize(['en', 'de']))
// todo set the active language based on the url
store.dispatch(addTranslation({
    resume: {
        link: ["BenediktFranke_Resume.pdf", "BenediktFranke_Lebenslauf.pdf"],
        title: ["Resume", "Lebenslauf"],
    },
    jobtitle: ["DevOps Engineer & Web Developer", "DevOps Spezialist & Webentwickler"],
    motto: [
        "I like to bring order to chaos. Structuring data, writing clean code and optimizing processes - I produce great results and learn along the way.",
        "Ich bringe gerne Ordnung ins Chaos. Daten strukturieren, sauberen Code schreiben und Prozesse optimieren - ich erziele gute Ergebnisse und lerne konstant dazu."
    ],
    links: {
        linkedIn: ["https://www.linkedin.com/in/benedikt-franke/?locale=en_US", "https://www.linkedin.com/in/benedikt-franke/?locale=de_DE"]
    }
}))

const root = () => (
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <ThemeProvider
                theme={{
                    fonts: {
                        sans: '"Courier New", Courier, monospace'
                    }
                }}
            >
                <App />
            </ThemeProvider>
        </ConnectedRouter>
    </Provider>
)

ReactDOM.render(root(), document.getElementById('root'));
registerServiceWorker();
