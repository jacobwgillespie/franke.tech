# franke.tech

This is the project for my personal homepage at https://franke.tech

I decided to open source this, maybe it will be useful to someone
and inspire them to make a site like this for themselves. This serves both as a description
of my progress and a reminder to myself of what i did here.

## Design

The page has a sleek, minimalist look. It is functional, rather than fancy - it serves the
simple purpose of pointing to a bunch of information about me. For the layout and UI elements,
i used the [Rebass](http://jxnblk.com/rebass/) component library.

## Resume generation

My resume is also contained in this project, written as plain markdown text files which are rendered to PDF by [markdown-resume](https://github.com/there4/markdown-resume).
While the base project did the job of rendering to PDF quite well, installation and especially usage in CI was quite painful
to set up, which is why i decided to contribute to the project and set up an [automated build on Docker Hub](https://hub.docker.com/r/there4/markdown-resume/)

## Deployment

Gitlab CI is used to compile the resume, build the files for the static site and package them in a Docker container.
This image is then deployed to a VM running a Rancher cluster.
A reverse proxy sits in front of it and handles SSL termination for it.

## Development

Bring up the development environment with: `docker-compose up -d`

View the log output with: `docker-compose logs -f`

To install new **node_modules**, bring down the stack with `docker-compose down` and run the node container with `docker run -it -v ${PWD}:/workdir node:alpine sh`.
