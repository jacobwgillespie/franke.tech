# Benedikt Franke
## DevOps Engineer, Web Developer

> [Download PDF](resume.pdf)  
> [benedikt@franke.tech](benedikt@franke.tech)  
> +49 174 1999723

------

### Profile {#profile}

I like to bring order to chaos.
Structuring data, writing clean code and optimizing processes - I produce great results and learn along the way.

------

### Skills {#skills}

* DevOps
  : Streamlining development processes and continuously delivering software through automation and containerization.

* Web Development
  : Proficient in designing and creating modern web applications that are both reliable and highly scalable.

* Project Direction
  : Able to engineer and manage projects independently or in a team and get things done the right way.

------

### Technology {#technology}

1. Docker
1. Kubernetes
1. Linux
1. Git
1. Infrastructure Automatisation
1. CI/CD
1. JavaScript
1. PHP
1. SQL
1. API Design
1. HTML
1. CSS

------

### Languages {#languages}

* German
  : Mother tongue

* English
  : Fluent while writing and speaking

* Spanish
  : Basic Knowledge

------

### Work {#work}

MLL Münchner Leukämielabor
: *Software Developer*
  __01/2018-Current__
  I am responsible for developing and modernizing the internal IT infrastructure and
  application landscape. My main focus is to architect the transformation of a
  legacy application towards a modern web application. I closely collaborate with users
  and plan, develop and maintain the system. By automating tests, configuration
  and deployment, i ensure quality and increase development speed.

twofour digitale Agentur GmbH
: *Web Developer*
  __01/2017-12/2017__
  I started an internship at twofour as part of my studies and was later employed full time.
  My Tasks were centered around PHP based CMS and shop frameworks. Through test-driven development
  in agile Scrum teams we rapidly delivered challenging software solutions to our customers.
  My responsibilities included project setup, backend and frontend development and optimization of our
  deployment pipeline.

Freudenberg Sealing Technologies
: *Freelance Web Developer*
  __06/2016-07/2016__
  Together with two colleagues we developed a web application used for automating the food
  order system of the companies cafeteria. After assessing the requirements of the project,
  i planned and set up the database and configured the application server. I automated the
  the export of the collected data and sending it to the catering provider.

Media Markt
: *Computer Salesman*
  __03/2013-02/2014__
  My areas of work included advising customers, providing technical service and
  placement of goods in the store. I was able to gather valuable experience
  through constant exposure to computers and refined my communication skills.

------

### Education {#education}

FH Kufstein - Web Business & Technology
: *Bachelor of Science in Engineering, graduated "with excellent success"*
  __09/2014-07/2017__
  This bachelor program deals with the basics of technological and economical knowledge
  required to design and create web-based information systems. The focus is set on imparting
  a fully qualified understanding of web-based business models, applications and web technologies.
  I learned principles of databases, programming and software engineering and applied them in
  two web application projects for the Kufstein based Freudenberg GmbH.

University of Regensburg
: *General Chemistry, no graduation*
  __10/2012-02/2013__

Annette-Kolb-Gymnasium Traunstein
: *High School, Final Grade: 2.2*
  __09/2004-06/2012__

------

### Footer {#footer}

Benedikt Franke -- [benedikt@franke.tech](benedikt@franke.tech) -- +49 174 1999723

------
