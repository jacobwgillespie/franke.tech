# Benedikt Franke
## DevOps Spezialist, Webentwickler

> [PDF herunterladen](resume.pdf)  
> [benedikt@franke.tech](benedikt@franke.tech)  
> +49 174 1999723

------

### Profil {#profile}

Ich bringe gerne Ordnung ins Chaos.
Daten strukturieren, sauberen Code schreiben und Prozesse optimieren - ich erziele gute Ergebnisse und lerne konstant dazu.

------

### Skills {#skills}

* DevOps
  : Optimierung der Entwicklung und kontinuierliche Auslieferung von Software durch Automatisierung und Container.

* Webentwicklung
  : Konzeption und Umsetzung moderner Web-Anwendungen, die sowohl zuverlässig als auch skalierbar sind.

* Projektleitung
  : Planung und Durchführung von komplexen Softwareprojekten, sowohl selbstständig als auch im Team.

------

### Technisch {#technisch}

1. Docker
1. Kubernetes
1. Linux
1. Git
1. Infrastructure Automatisation
1. CI/CD
1. JavaScript
1. PHP
1. SQL
1. API Design
1. HTML
1. CSS

------

### Arbeit {#Arbeit}

MLL Münchner Leukämielabor
: *Softwareentwickler*
  __01/2018-Aktuell__
  Ich bin verantwortlich für die Weiterentwicklung und Modernisierung der internen IT-Infrastruktur und verwendeten Anwendungen.
  Dabei arbeite ich eng mit den Nutzern zusammen und plane, entwickle und verwalte das System.  
  Mein Fokus liegt auf der Neugestaltung eines bestehenden Altsystems und dessen Transformation zu einer modernen Webanwendung.
  Durch die Automatisierung von Softwaretests, Konfiguration und Deployment stelle ich die Qualität der Anwendung sicher und beschleunige die Entwicklung.

twofour digitale Agentur GmbH
: *Webentwickler*
  __01/2017-12/2017__
  Als Teil meines Studiums begann ich bei twofour ein Praktikum, welches anschließend in eine Festanstellung überging.
  Dort beschäftigte ich mich hauptsächlich mit PHP basierten CMS und Shop-Frameworks.
  Durch den Einsatz von testgetriebener Entwicklung im agilen Scrum-Umfeld lieferten wir herausfordernde Softwarelösungen an unsere Kunden.  
  Meine Aufgaben beinhalteten das Setup von Projekten, Entwicklung in Backend und Frontend sowie die Optimierung unserer Deploymentprozesse.

Freudenberg Sealing Technologies
: *Freelance Webentwickler*
  __06/2016-07/2016__
  Gemeinsam mit zwei Kommilitonen entwickelten wir eine Webanwendung für die automatisierte Bestellung
  von Essen in der betriebsinternen Kantine. Nachdem wir die Anforderungen für das Projekt erhoben hatten,
  plante und erstellte ich das Datenbankschema und konfigurierte den Anwendungsserver. Ich automatisierte
  den Export der eingegebenen Daten und deren Übermittlung an den Lieferdienst.

Media Markt
: *Verkäufer Computerabteilung*
  __03/2013-02/2014__
  Meine Aufgaben bestanden in der Beratung von Kunden, Bereitstellung technischer Serviceleistung
  sowie die Präsentation von Waren im Geschäft. Ich konnte wertvolle Erfahrungen sammeln durch
  die Arbeit mit technischen Geräten und den persönlichen Kontakt zu Kunden.

------

### Ausbildung {#ausbildung}

FH Kufstein - Web Business & Technology
: *Bachelor of Science in Engineering  
graduated "with excellent success"*
  __09/2014-07/2017__
  Inhalt dieses Bachelorstudiums sind die wirtschaftlichen und technologischen Grundlagen webbasierter Informationssysteme.
  Der Fokus liegt auf einem ganzheitlichen Verständnis webbasierte Geschäftsmodelle, Anwendungen und Technologien des Web.
  Hierbei lernte ich die Prinzipien von Datenbanken, Programmierung und Software Engineering und konnte diese in zwei Praxisprojekten in Kooperation mit der Freudenberg GmbH in Kufstein umsetzen.

Universität Regensburg
: *Allgemeine Chemie  
ohne Abschluss*
  __10/2012-02/2013__

Annette-Kolb-Gymnasium Traunstein
: *Abitur  
Abschlussnote 2,2*
  __09/2004-06/2012__

------

### Sprachen {#sprachen}

* Deutsch
  : Muttersprache

* Englisch
  : Fließend in Wort und Schrift

* Spanisch
  : Grundkenntnisse

------

### Footer {#footer}

Benedikt Franke -- [benedikt@franke.tech](benedikt@franke.tech) -- +49 174 1999723

------
